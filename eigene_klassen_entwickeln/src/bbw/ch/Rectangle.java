package bbw.ch;

public class Rectangle {
    private int width, height;

    public Rectangle(int width, int height){
        setWidth(width);
        setHeight(height);
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void draw(){
        StringBuilder t = new StringBuilder();
        for (int i = 0; i < height; i++){
            t.append("*".repeat(Math.max(0, width)));
            System.out.println(t);
            t.setLength(0);
        }
        System.out.println();
    }

}

package bbw.ch;

public class Square {
    private int size;

    public Square(int size){
        setSize(size);
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void draw(){
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < size; i++){
            text.append("*".repeat(size));
            System.out.println(text);
            text.setLength(0);
        }
        System.out.println();
    }

}

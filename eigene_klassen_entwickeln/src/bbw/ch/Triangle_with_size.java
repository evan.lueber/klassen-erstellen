package bbw.ch;

public class Triangle_with_size {
    private int size;

    public Triangle_with_size(int size){
        setSize(size);
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void draw(){
    }


}

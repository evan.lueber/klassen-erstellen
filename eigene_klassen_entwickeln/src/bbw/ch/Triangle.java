package bbw.ch;

public class Triangle {
    private int height;

    public Triangle(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public  void draw(){
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < height; i++){
            for (int a = 0; a < i + 1; a++){
                if(a == 0){
                    text = new StringBuilder("|");
                }else if(a == i){
                    text.append("\\");
                }
                else {
                    text.append("*");
                }
            }
            System.out.println(text);
            text = new StringBuilder();
        }
        System.out.println();
    }

}
